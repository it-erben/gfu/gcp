# Demo setup for GCE Monitoring

Das Shell-Skript in diesem Verzeichnis setzt drei GCE VMs auf, die hohe CPU-Auslastung haben.
Mit ihnen können wir demonstrieren, wie GCE-Instanzen auf Dashboards zu sehen sind und wie wir
Alerts für sie anlegen können.
